package com.enricbala.servicios;

import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    static MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbprod");
        return database.getCollection("servicios");
    }
    public static void insert(String servicio) throws Exception {
        Document doc = Document.parse(servicio);
        servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }

    public static List getAll() {
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            lst.add(it.next());
        }
        return lst;
    }

    public static void update(String filtro, String servicio) {
        servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document doc = Document.parse(servicio);
        servicios.updateOne(docFiltro, doc);
    }


}
