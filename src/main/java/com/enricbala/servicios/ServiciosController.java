package com.enricbala.servicios;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")  //Atención
public class ServiciosController {
    @GetMapping("/servicios")
    public List getServicios() {
        return ServiciosService.getAll();
    }
    @PostMapping("/servicios")
    public String setServicio(@RequestBody String newServicio) {
        try {
            ServiciosService.insert(newServicio);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }




}
